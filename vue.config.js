module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
        ? '/balanco-patrimonial-e-dre/'
        : '/',
    productionSourceMap: false,
}
  